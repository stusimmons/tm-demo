package nz.co.stuarts.trademe

import io.reactivex.Single
import nz.co.stuarts.trademe.R.string.categories
import nz.co.stuarts.trademe.common.TestDataFactory
import nz.co.stuarts.trademe.data.DataManager
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.categories.CategoriesMvpView
import nz.co.stuarts.trademe.features.listings.list.ListingsMvpView
import nz.co.stuarts.trademe.features.listings.list.ListingsPresenter
import nz.co.stuarts.trademe.features.main.CategoriesPresenter
import nz.co.stuarts.trademe.util.RxSchedulersOverrideRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ravindra on 24/12/16.
 */
@RunWith(MockitoJUnitRunner::class)
class ListingsPresenterTest {

    @Mock lateinit var mMockListingMvpView: ListingsMvpView
    @Mock lateinit var mMockDataManager: DataManager
    private var mPresenter: ListingsPresenter? = null

    @JvmField
    @Rule val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = ListingsPresenter(mMockDataManager)
        mPresenter?.attachView(mMockListingMvpView)
    }

    @After
    fun tearDown() {
        mPresenter?.detachView()
    }

    @Test
    @Throws(Exception::class)
    fun getListingsReturnsListings() {
        val listings = TestDataFactory.makeListings()
        `when`(mMockDataManager.getListingsForCategory("0"))
                .thenReturn(Single.just(listings))

        mPresenter?.getListings("0")
        verify<ListingsMvpView>(mMockListingMvpView, times(2)).showProgress(anyBoolean())
        verify<ListingsMvpView>(mMockListingMvpView).showListings(listings)
        verify<ListingsMvpView>(mMockListingMvpView, never()).showError(RuntimeException())
    }

    @Test
    @Throws(Exception::class)
    fun getListingsReturnsEmpty() {
        val listings = listOf<ListItem>()
        `when`(mMockDataManager.getListingsForCategory("0"))
                .thenReturn(Single.just(listings))

        mPresenter?.getListings("0")
        verify<ListingsMvpView>(mMockListingMvpView, times(2)).showProgress(anyBoolean())
        verify<ListingsMvpView>(mMockListingMvpView).showListingsEmpty()
        verify<ListingsMvpView>(mMockListingMvpView, never()).showError(RuntimeException())
    }

}