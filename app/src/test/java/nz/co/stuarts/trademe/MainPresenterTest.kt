package nz.co.stuarts.trademe

import nz.co.stuarts.trademe.features.main.MainMvpView
import nz.co.stuarts.trademe.features.main.MainPresenter
import nz.co.stuarts.trademe.util.RxSchedulersOverrideRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ravindra on 24/12/16.
 */
@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

    @Mock lateinit var mMockMainMvpView: MainMvpView
    private var mPresenter: MainPresenter? = null

    @JvmField
    @Rule val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = MainPresenter()
        mPresenter?.attachView(mMockMainMvpView)
    }

    @After
    fun tearDown() {
        mPresenter?.detachView()
    }

    @Test
    @Throws(Exception::class)
    fun getListingsReturnsListings() {
        mPresenter?.defaultCategory()
        verify<MainMvpView>(mMockMainMvpView).showSubcategories("")
        verify<MainMvpView>(mMockMainMvpView, never()).showError(RuntimeException())
    }
}