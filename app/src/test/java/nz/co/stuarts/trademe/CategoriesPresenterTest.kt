package nz.co.stuarts.trademe

import nz.co.stuarts.trademe.common.TestDataFactory
import nz.co.stuarts.trademe.data.DataManager
import nz.co.stuarts.trademe.util.RxSchedulersOverrideRule
import io.reactivex.Single
import nz.co.stuarts.trademe.features.categories.CategoriesMvpView
import nz.co.stuarts.trademe.features.main.CategoriesPresenter
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ravindra on 24/12/16.
 */
@RunWith(MockitoJUnitRunner::class)
class CategoriesPresenterTest {

    @Mock lateinit var mMockCategoriesMvpView: CategoriesMvpView
    @Mock lateinit var mMockDataManager: DataManager
    private var mPresenter: CategoriesPresenter? = null

    @JvmField
    @Rule val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mPresenter = CategoriesPresenter(mMockDataManager)
        mPresenter?.attachView(mMockCategoriesMvpView)
    }

    @After
    fun tearDown() {
        mPresenter?.detachView()
    }

    @Test
    @Throws(Exception::class)
    fun getCategoriesReturnsSubcategoriesList() {
        val categories = TestDataFactory.makeCategories()
        `when`(mMockDataManager.getCategories())
                .thenReturn(Single.just(categories))

        mPresenter?.checkConnection(true)
        verify<CategoriesMvpView>(mMockCategoriesMvpView).showConnected()
        mPresenter?.getCategories("")
        verify<CategoriesMvpView>(mMockCategoriesMvpView, times(2)).showProgress(anyBoolean())
        verify<CategoriesMvpView>(mMockCategoriesMvpView).showCategories(categories)
        verify<CategoriesMvpView>(mMockCategoriesMvpView, never()).showError(RuntimeException())
    }

    @Test
    @Throws(Exception::class)
    fun getCategoriesNoConnection() {
        val categories = TestDataFactory.makeCategories()

        mPresenter?.checkConnection(false)
        verify<CategoriesMvpView>(mMockCategoriesMvpView).showNoConnection()
        verify<CategoriesMvpView>(mMockCategoriesMvpView, never()).showCategories(categories)
    }

    @Test
    @Throws(Exception::class)
    fun getNestedCategories() {
        val categories = TestDataFactory.makeCategories()
        `when`(mMockDataManager.getCategories())
                .thenReturn(Single.just(categories))

        val subCategory = TestDataFactory.makeSubcategory()
        `when`(mMockDataManager.getCategory("1"))
                .thenReturn(Single.just(subCategory))

        mPresenter?.checkConnection(true)
        mPresenter?.getCategories("")
        verify<CategoriesMvpView>(mMockCategoriesMvpView, times(2)).showProgress(anyBoolean())
        verify<CategoriesMvpView>(mMockCategoriesMvpView).showCategories(categories)
        mPresenter?.getCategories("1")
        subCategory.Subcategories?.let { verify<CategoriesMvpView>(mMockCategoriesMvpView).showCategories(it) }

        verify<CategoriesMvpView>(mMockCategoriesMvpView, never()).showNoConnection()
    }
}