package nz.co.stuarts.trademe.common.injection.component

import dagger.Component
import nz.co.stuarts.trademe.common.injection.module.ApplicationTestModule
import nz.co.stuarts.trademe.injection.component.AppComponent
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationTestModule::class))
interface TestComponent : AppComponent