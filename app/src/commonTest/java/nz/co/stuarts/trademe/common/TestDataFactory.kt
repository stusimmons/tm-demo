package nz.co.stuarts.trademe.common

import com.google.gson.Gson
import nz.co.stuarts.trademe.data.model.categories.Category
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.data.model.search.Listings

/**
 * Factory class that makes instances of data models with random field values.
 * The aim of this class is to help setting up test fixtures.
 */
object TestDataFactory {

    fun makeCategories(): List<Subcategory> {

        val json = """{
            "Name": "Root",
            "Number": "",
            "Path": "",
            "Subcategories": [{
            "Name": "Trade Me Motors",
            "Number": "0001-",
            "Path": "\/Trade-Me-Motors",
            "HasClassifieds": true,
            "CanHaveSecondCategory": true,
            "CanBeSecondCategory": true,
            "IsLeaf": false
        }, {
            "Name": "Trade Me Property",
            "Number": "0350-",
            "Path": "\/Trade-Me-Property",
            "HasClassifieds": true,
            "IsLeaf": false
        }, {
            "Name": "Trade Me Jobs",
            "Number": "5000-",
            "Path": "\/Trade-Me-Jobs",
            "HasClassifieds": true,
            "IsLeaf": false
        }],
            "IsLeaf": false
        }"""

        val gson = Gson()
        val categories = gson.fromJson<Category>(json, Category::class.java)
        return categories.Subcategories
    }

    fun makeSubcategory(): Subcategory {

        val json = """{
        	"Name": "Books",
        	"Number": "0193-",
        	"Path": "\/Books",
        	"Subcategories": [{
        		"Name": "Audio books",
        		"Number": "0193-0461-",
        		"Path": "\/Books\/Audio-books",
        		"Subcategories": [{
        			"Name": "CD",
        			"Number": "0193-0461-4062-",
        			"Path": "\/Books\/Audio-books\/CD",
        			"Subcategories": [{
        				"Name": "Children's",
        				"Number": "0193-0461-4062-9201-",
        				"Path": "\/Books\/Audio-books\/CD\/Childrens",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}, {
        				"Name": "Fiction",
        				"Number": "0193-0461-4062-9203-",
        				"Path": "\/Books\/Audio-books\/CD\/Fiction",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}, {
        				"Name": "Non-fiction",
        				"Number": "0193-0461-4062-9202-",
        				"Path": "\/Books\/Audio-books\/CD\/Nonfiction",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}, {
        				"Name": "Other",
        				"Number": "0193-0461-4062-9204-",
        				"Path": "\/Books\/Audio-books\/CD\/Other",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}],
        			"CanHaveSecondCategory": true,
        			"CanBeSecondCategory": true,
        			"IsLeaf": false
        		}, {
        			"Name": "Tape",
        			"Number": "0193-0461-4061-",
        			"Path": "\/Books\/Audio-books\/Tape",
        			"AreaOfBusiness": 1,
        			"CanHaveSecondCategory": true,
        			"CanBeSecondCategory": true,
        			"IsLeaf": true
        		}],
        		"CanHaveSecondCategory": true,
        		"CanBeSecondCategory": true,
        		"IsLeaf": false
        	}, {
        		"Name": "Bulk",
        		"Number": "0193-6344-",
        		"Path": "\/Books\/Bulk",
        		"AreaOfBusiness": 1,
        		"CanHaveSecondCategory": true,
        		"CanBeSecondCategory": true,
        		"IsLeaf": true
        	}, {
        		"Name": "Children & babies",
        		"Number": "0193-0462-",
        		"Path": "\/Books\/Children-babies",
        		"Subcategories": [{
        			"Name": "Activity & colouring",
        			"Number": "0193-0462-6718-",
        			"Path": "\/Books\/Children-babies\/Activity-colouring",
        			"Subcategories": [{
        				"Name": "Colouring",
        				"Number": "0193-0462-6718-9733-",
        				"Path": "\/Books\/Children-babies\/Activity-colouring\/Colouring",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}, {
        				"Name": "Puzzles & games",
        				"Number": "0193-0462-6718-9731-",
        				"Path": "\/Books\/Children-babies\/Activity-colouring\/Puzzles-games",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}, {
        				"Name": "Sticker",
        				"Number": "0193-0462-6718-9732-",
        				"Path": "\/Books\/Children-babies\/Activity-colouring\/Sticker",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}, {
        				"Name": "Other",
        				"Number": "0193-0462-6718-9734-",
        				"Path": "\/Books\/Children-babies\/Activity-colouring\/Other",
        				"AreaOfBusiness": 1,
        				"CanHaveSecondCategory": true,
        				"CanBeSecondCategory": true,
        				"IsLeaf": true
        			}],
        			"CanHaveSecondCategory": true,
        			"CanBeSecondCategory": true,
        			"IsLeaf": false
        		}],
        		"CanHaveSecondCategory": true,
        		"CanBeSecondCategory": true,
        		"IsLeaf": false
        	}]

        }"""

        val gson = Gson()
        val categories = gson.fromJson<Category>(json, Category::class.java)
        return categories.Subcategories[0]
    }

    fun makeListings(): List<ListItem> {

        val json = """{
        	"TotalCount": 2610,
        	"Page": 1,
        	"PageSize": 20,
        	"List": [{
        		"__type": "Property:http:\/\/api.trademe.co.nz\/v1",
        		"ListingId": 6108991,
        		"Title": "Big House on Big Land in New Lynn Heights",
        		"Category": "0350-5748-3399-",
        		"StartPrice": 0,
        		"StartDate": "\/Date(1509064031133)\/",
        		"EndDate": "\/Date(1513902971133)\/",
        		"ListingLength": null,
        		"IsFeatured": true,
        		"HasGallery": true,
        		"IsBold": true,
        		"IsHighlighted": true,
        		"AsAt": "\/Date(1513473339852)\/",
        		"CategoryPath": "\/Trade-Me-Property\/Residential\/For-Sale",
        		"PictureHref": "https:\/\/images.tmsandbox.co.nz\/photoserver\/thumb\/3798219.jpg",
        		"RegionId": 1,
        		"Region": "Auckland",
        		"SuburbId": 307,
        		"Suburb": "New Lynn",
        		"NoteDate": "\/Date(0)\/",
        		"ReserveState": 3,
        		"IsClassified": true,
        		"OpenHomes": [],
        		"GeographicLocation": {
        			"Latitude": -36.918547,
        			"Longitude": 174.6695797,
        			"Northing": 5912862,
        			"Easting": 1748717,
        			"Accuracy": 1
        		},
        		"PriceDisplay": "To be auctioned",
        		"IsSuperFeatured": true,
        		"PhotoUrls": ["https:\/\/images.tmsandbox.co.nz\/photoserver\/thumb\/3798220.jpg", "https:\/\/images.tmsandbox.co.nz\/photoserver\/thumb\/3798221.jpg"],
        		"AdditionalData": {
        			"BulletPoints": [],
        			"Tags": []
        		},
        		"Address": "1A Trojan Crescent",
        		"District": "Waitakere City",
        		"LandArea": 808,
        		"Bathrooms": 1,
        		"Bedrooms": 4,
        		"ListingGroup": "PROPERTY",
        		"Parking": "2 Car Garage",
        		"PropertyType": "House",
        		"AdjacentSuburbNames": ["Avondale", "Kelston", "New Lynn", "Green Bay"],
        		"AdjacentSuburbIds": [149, 263, 307, 308],
        		"DistrictId": 6,
        		"Agency": {
        			"Id": 1,
        			"Name": "test, Licensed Agent (REAA 2008)",
        			"PhoneNumber": "+64-1-1234567",
        			"Branding": {
        				"BackgroundColor": "",
        				"TextColor": "",
        				"StrokeColor": "",
        				"OfficeLocation": "",
        				"LargeBannerURL": ""
        			},
        			"Agents": [{
        				"FullName": "Wayne Zhang",
        				"MobilePhoneNumber": "(027) 5828838",
        				"OfficePhoneNumber": "(09) 3744329"
        			}],
        			"IsRealEstateAgency": true,
        			"IsLicensedPropertyAgency": true
        		}
        	}],
        	"DidYouMean": "",
        	"FoundCategories": []
        }"""

        val gson = Gson()
        val categories = gson.fromJson<Listings>(json, Listings::class.java)
        return categories.List
    }}