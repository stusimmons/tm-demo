package nz.co.stuarts.trademe.features.main

import nz.co.stuarts.trademe.R
import nz.co.stuarts.trademe.features.common.ErrorView
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import nz.co.stuarts.trademe.features.base.BaseFragment
import timber.log.Timber
import javax.inject.Inject
import android.view.ViewGroup
import android.view.LayoutInflater
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.listings.list.ListingsMvpView
import nz.co.stuarts.trademe.features.listings.list.ListingsPresenter

class ListingsFragment : BaseFragment(), ListingsMvpView, ListingsAdapter.ClickListener, ErrorView.ErrorListener {
    @Inject lateinit var mListingsAdapter: ListingsAdapter
    @Inject lateinit var mPresenter: ListingsPresenter

    @BindView(R.id.view_error) @JvmField var mErrorView: ErrorView? = null
    @BindView(R.id.progress) @JvmField var mProgress: ProgressBar? = null
    @BindView(R.id.recycler) @JvmField var mListingsRecyler: RecyclerView? = null
    @BindView(R.id.swipe_to_refresh) @JvmField var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    var category: String = ""

    var notifyListings = PublishSubject.create<ListItem>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(layout, container, false)
        fragmentComponent().inject(this)
        ButterKnife.bind(this, view)

        mPresenter.attachView(this)

        readBundle(arguments)
        mSwipeRefreshLayout?.setProgressBackgroundColorSchemeResource(R.color.primary)
        mSwipeRefreshLayout?.setColorSchemeResources(R.color.white)
        mSwipeRefreshLayout?.setOnRefreshListener { mPresenter.getListings(category)}
        mListingsRecyler?.adapter = mListingsAdapter
        mListingsAdapter.setClickListener(this)
        mListingsRecyler?.layoutManager = LinearLayoutManager(context)
        mErrorView?.setErrorListener(this)

        mPresenter.getListings(category)
        return view
    }

    private fun readBundle(bundle: Bundle?) {
        if (bundle != null) {
            category = bundle.getString("category")
        }
    }

    companion object {

        fun newInstance(category: String): ListingsFragment {
            val bundle = Bundle()
            bundle.putString("category", category)
            val fragment = ListingsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override val layout: Int
        get() = R.layout.fragment_recycler

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    override fun onReloadData() {
        mPresenter.getListings(category)
    }

    override fun showListings(listings: List<ListItem>) {
        mListingsAdapter.setListings(listings)
        mListingsAdapter.notifyDataSetChanged()

        mListingsRecyler?.visibility = View.VISIBLE
        mSwipeRefreshLayout?.visibility = View.VISIBLE
    }

    override fun showListingsEmpty() {
        mListingsRecyler?.visibility = View.GONE
        mErrorView?.visibility = View.VISIBLE
        mErrorView?.setMessage(getString(R.string.error_no_elements))
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            if (mListingsRecyler?.visibility == View.VISIBLE && mListingsAdapter.itemCount > 0) {
                mSwipeRefreshLayout?.isRefreshing = true
            } else {
                mProgress?.visibility = View.VISIBLE

                mListingsRecyler?.visibility = View.GONE
                mSwipeRefreshLayout?.visibility = View.GONE
            }

            mErrorView?.visibility = View.GONE
        } else {
            mSwipeRefreshLayout?.isRefreshing = false
            mProgress?.visibility = View.GONE
        }
    }

    override fun showError(error: Throwable) {
        mListingsRecyler?.visibility = View.GONE
        mSwipeRefreshLayout?.visibility = View.GONE
        mErrorView?.visibility = View.VISIBLE
        Timber.e(error, "There was an error retrieving the pokemon")
    }

    override fun onItemClick(listing: ListItem) {
        notifyListings.onNext(listing)
    }

    fun listingsObservable(): Observable<ListItem> {
        return notifyListings
    }
}