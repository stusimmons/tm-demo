package nz.co.stuarts.trademe.data.remote

import io.reactivex.Single
import nz.co.stuarts.trademe.data.model.categories.Category
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.data.model.search.Listings
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TradeMeApi {

    @GET("v1/Categories/0.json")
    fun getCategories(@Query("depth") depth: Int): Single<Category>

    @GET("v1/Categories/{number}.json")
    fun getSubcategory(@Path("number") number: String): Single<Subcategory>

    @GET("v1/Search/General.json?rows=20")
    fun getListings(@Query("category") category: String): Single<Listings>
}
