package nz.co.stuarts.trademe.data.model.categories

/**
 * Created by stu on 17/12/17.
 */
data class Category(
        val Name: String, //Root
        val Number: String,
        val Path: String,
        val Subcategories: List<Subcategory>,
        val IsLeaf: Boolean //false
)