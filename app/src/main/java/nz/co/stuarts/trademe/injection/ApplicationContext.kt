package nz.co.stuarts.trademe.injection


import javax.inject.Qualifier

@Qualifier @Retention annotation class ApplicationContext
