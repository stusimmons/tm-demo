package nz.co.stuarts.trademe.features.listings

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.widget.Toolbar
import android.widget.FrameLayout
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.Observable
import nz.co.stuarts.trademe.R
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.base.BaseActivity
import nz.co.stuarts.trademe.features.common.ErrorView
import nz.co.stuarts.trademe.features.listings.listing.detail.DetailFragment
import nz.co.stuarts.trademe.features.main.ListingsFragment
import javax.inject.Inject

class BrowseActivity : BaseActivity(), BrowseMvpView {

    private val TAG_LISTINGS_FRAGMENT = "listings_fragment"
    private val TAG_LISTINGS_DETAIL_FRAGMENT = "listings_detail_fragment"

    private var listingsFragment: ListingsFragment? = null
    private var listingDetailFragment: DetailFragment? = null

    @Inject lateinit var mPresenter: BrowsePresenter

    @BindView(R.id.view_error) @JvmField var mErrorView: ErrorView? = null
    @BindView(R.id.progress) @JvmField var mProgress: ProgressBar? = null
    @BindView(R.id.toolbar) @JvmField var mToolbar: Toolbar? = null
    @BindView(R.id.single_pane) @JvmField var mLeftFrame: FrameLayout? = null
    @BindView(R.id.dual_pane) @JvmField var mRightFrame: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        setContentView(layout)
        ButterKnife.bind(this)

        mPresenter.attachView(this)
        mToolbar?.title = intent.getStringExtra("title")
        setSupportActionBar(mToolbar)

        if (null == savedInstanceState) {
            listingsFragment = ListingsFragment.newInstance(intent.getStringExtra("category"))
            attachFragments()
        } else {
            listingsFragment = supportFragmentManager.findFragmentByTag(TAG_LISTINGS_FRAGMENT) as ListingsFragment
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter.addListingListener(listingsFragment?.listingsObservable() as Observable<ListItem>)
    }

    override fun showListingDetail(listing: ListItem) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        listingDetailFragment = DetailFragment.newInstance(listing)

        if (mRightFrame != null) {
            fragmentTransaction.replace(R.id.dual_pane, listingDetailFragment, TAG_LISTINGS_DETAIL_FRAGMENT)
            fragmentTransaction.commitAllowingStateLoss()
        } else {
            launchSinglePaneListing()
        }
    }

    private fun launchSinglePaneListing() {
        val fm = supportFragmentManager
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val detailFragment = listingDetailFragment
        val fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.single_pane, detailFragment, TAG_LISTINGS_DETAIL_FRAGMENT)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
    private fun attachFragments() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.single_pane, listingsFragment, TAG_LISTINGS_FRAGMENT)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun finish() {
        super.finish()
        this.overridePendingTransition(0, 0)
    }

    override val layout: Int
        get() = R.layout.activity_browse
}