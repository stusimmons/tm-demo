package nz.co.stuarts.trademe.data

import io.reactivex.Single
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.data.remote.TradeMeApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject
constructor(private val mTradeMeApi: TradeMeApi) {

    fun getCategories(): Single<List<Subcategory>> {
        return mTradeMeApi.getCategories(2) //change this to control how many menus deep you go.
                .map { it.Subcategories }
                .cache()
    }
    fun getCategory(number: String): Single<Subcategory> {
        return mTradeMeApi.getSubcategory(number)
                .cache()
    }

    fun getListingsForCategory(category: String): Single<List<ListItem>> {
        return mTradeMeApi.getListings(category)
                .map{ it.List }
    }
}