package nz.co.stuarts.trademe.features.categories

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import javax.inject.Inject

class CategoriesAdapter @Inject
constructor() : RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>() {

    private var mCategories: List<Subcategory>? = null
    private var mClickListener: ClickListener? = null

    init {
        mCategories = emptyList()
    }

    fun setCategories(categories: List<Subcategory>) {
        mCategories = categories
    }

    fun setClickListener(clickListener: ClickListener) {
        mClickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false)
        return CategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        val category = mCategories?.get(position)
        holder.mCategory = category
        holder.nameText?.text = category?.Name
    }

    override fun getItemCount(): Int {
        return mCategories?.size as Int
    }

    interface ClickListener {
        fun onItemClick(callback: Subcategory?)
    }

    open inner class CategoriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mCategory: Subcategory? = null
        @BindView(android.R.id.text1) @JvmField var nameText: TextView? = null

        init {
            ButterKnife.bind(this, itemView)
            itemView.setOnClickListener { if (mClickListener != null) mClickListener?.onItemClick(mCategory) }
        }
    }
}
