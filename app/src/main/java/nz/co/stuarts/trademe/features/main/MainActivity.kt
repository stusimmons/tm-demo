package nz.co.stuarts.trademe.features.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import nz.co.stuarts.trademe.R
import nz.co.stuarts.trademe.features.base.BaseActivity
import nz.co.stuarts.trademe.features.categories.CategoriesFragment
import nz.co.stuarts.trademe.features.listings.BrowseActivity
import timber.log.Timber
import javax.inject.Inject

class MainActivity : BaseActivity(), MainMvpView {

    private val TAG_CATEGORIES_FRAGMENT = "categories_fragment"

    @BindView(R.id.toolbar) @JvmField var mToolbar: Toolbar? = null

    @Inject lateinit var mMainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        mMainPresenter.attachView(this)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        mToolbar?.title = getString(R.string.categories)
        setSupportActionBar(mToolbar)

        if (savedInstanceState == null) {
            //if creating the activity, show top level
            mMainPresenter.defaultCategory()
        } else {
            //if recreating, add listeners to each fragment
            for (tag: String in mMainPresenter.getFragments()){
                restoreFragment(tag)
            }
        }
    }

    private fun restoreFragment(tag: String){

        val categoriesFragment = supportFragmentManager.findFragmentByTag(tag)
        if (categoriesFragment is CategoriesFragment) {
            mMainPresenter.addCategoryListener(categoriesFragment.categoriesObservable())
        }
    }

    override val layout: Int
        get() = R.layout.activity_main

    override fun onDestroy() {
        super.onDestroy()
        mMainPresenter.detachView()
    }

    override fun showListingsForCategory(id: String?, title: String?) {
        val intent = Intent(this, BrowseActivity::class.java)
        intent.putExtra("category", id)
        intent.putExtra("title", title)
        startActivity(intent)
        this.overridePendingTransition(0, 0)
    }

    override fun showSubcategories(subcategories: String) {

        val tag = TAG_CATEGORIES_FRAGMENT + subcategories
        mMainPresenter.addFragment(tag)

        val fm = supportFragmentManager
        val categoriesFragment = CategoriesFragment.newInstance(subcategories)
        val fragmentTransaction = fm.beginTransaction()
        fragmentTransaction.replace(R.id.categories_fragment, categoriesFragment, tag)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
        mMainPresenter.addCategoryListener(categoriesFragment.categoriesObservable())
    }

    override fun showError(error: Throwable) {
        Timber.e(error, "There was an error retrieving the categories")
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 1) {
            fm.popBackStack()
        } else {
            super.onBackPressed()
            finish()
        }
    }
}