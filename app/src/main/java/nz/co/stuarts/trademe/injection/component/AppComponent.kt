package nz.co.stuarts.trademe.injection.component

import android.app.Application
import android.content.Context
import dagger.Component
import nz.co.stuarts.trademe.data.DataManager
import nz.co.stuarts.trademe.data.remote.TradeMeApi
import nz.co.stuarts.trademe.injection.ApplicationContext
import nz.co.stuarts.trademe.injection.module.AppModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    @ApplicationContext
    fun context(): Context

    fun application(): Application

    fun dataManager(): DataManager

    fun pokemonApi(): TradeMeApi
}
