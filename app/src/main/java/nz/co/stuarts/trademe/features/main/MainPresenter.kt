package nz.co.stuarts.trademe.features.main

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.features.base.BasePresenter
import nz.co.stuarts.trademe.injection.ConfigPersistent
import nz.co.stuarts.trademe.util.rx.scheduler.SchedulerUtils
import timber.log.Timber
import javax.inject.Inject


@ConfigPersistent
class MainPresenter @Inject
constructor() : BasePresenter<MainMvpView>() {

    private var subscriptions: CompositeDisposable? = null

    private var fragments: List<String> = listOf()

    override fun attachView(mvpView: MainMvpView) {
        super.attachView(mvpView)
        subscriptions = CompositeDisposable()
    }

    override fun detachView() {
        super.detachView()
        subscriptions?.clear()
    }

    fun defaultCategory(){
        mvpView?.showSubcategories("")
    }

    fun addCategoryListener(listener: Observable<Subcategory>) {
        subscriptions?.add(listener
                .compose(SchedulerUtils.ioToMain<Subcategory>())
                .subscribe(
                        {
                            if (it.Subcategories != null) {
                                mvpView?.showSubcategories(it.Number)
                            } else {
                                mvpView?.showListingsForCategory(it.Number, it.Name)
                            }
                        },
                        { error -> Timber.e(error) }
                ))
    }

    fun addFragment(tag: String) {
        fragments += tag
    }

    fun getFragments() : List<String> {
        return fragments
    }
}