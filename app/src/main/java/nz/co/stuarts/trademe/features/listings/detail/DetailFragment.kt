package nz.co.stuarts.trademe.features.listings.listing.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import nz.co.stuarts.trademe.R
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.base.BaseFragment
import nz.co.stuarts.trademe.features.common.ErrorView
import nz.co.stuarts.trademe.util.loadImageFromUrl

class DetailFragment : BaseFragment() {


    @BindView(R.id.view_error) @JvmField var mErrorView: ErrorView? = null
    @BindView(R.id.progress) @JvmField var mProgress: ProgressBar? = null

    @BindView(R.id.image) @JvmField var mImage: ImageView? = null
    @BindView(R.id.text_heading) @JvmField var mHeading: TextView? = null
    @BindView(R.id.text_subheading) @JvmField var mSubheading: TextView? = null
    @BindView(R.id.text_listing_id) @JvmField var mListingId: TextView? = null

    var listing: ListItem? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(layout, container, false)
        fragmentComponent().inject(this)
        ButterKnife.bind(this, view)

        showListing(readBundle(arguments))
        return view
    }

    private fun readBundle(bundle: Bundle?): ListItem? {
        if (bundle != null) {
            listing = bundle.getParcelable("listing")
        }
        return listing
    }

    private fun showListing(listItem: ListItem?){
        if (listItem?.Subtitle.isNullOrBlank()) {
            mSubheading?.visibility = View.GONE
        } else {
            mSubheading?.text = listItem?.Subtitle
        }
        mHeading?.text = listItem?.Title
        mListingId?.text = listItem?.ListingId.toString()
        listItem?.PictureHref?.let { mImage?.loadImageFromUrl(it) }
    }

    companion object {

        fun newInstance(listing: ListItem): DetailFragment {
            val bundle = Bundle()
            bundle.putParcelable("listing", listing)
            val fragment = DetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override val layout: Int
        get() = R.layout.fragment_detail
}