package nz.co.stuarts.trademe.features.main

import nz.co.stuarts.trademe.features.base.MvpView

interface MainMvpView : MvpView {

    fun showError(error: Throwable)

    fun showSubcategories(subcategories: String)

    fun showListingsForCategory(id: String?, title: String?)

}