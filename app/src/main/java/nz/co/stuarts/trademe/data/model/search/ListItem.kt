package nz.co.stuarts.trademe.data.model.search

import android.os.Parcel
import android.os.Parcelable

data class ListItem(val PictureHref: String = "",
                    val AdditionalData: AdditionalData?,
                    val Category: String = "",
                    val ListingLength: Int = 0,
                    val Suburb: String = "",
                    val PromotionId: Int = 0,
                    val BuyNowPrice: Double = 0.0,
                    val Title: String = "",
                    val IsNew: Boolean = false,
                    val HasBuyNow: Boolean = false,
                    val EndDate: String = "",
                    val IsBuyNowOnly: Boolean = false,
                    val StartDate: String = "",
                    val ReserveState: Int = 0,
                    val NoteDate: String = "",
                    val Subtitle: String = "",
                    val CategoryPath: String = "",
                    val ListingId: Int = 0,
                    val Region: String = "",
                    val HasGallery: Boolean = false,
                    val AsAt: String = "",
                    val PriceDisplay: String = "",
                    val StartPrice: Double = 0.0) : Parcelable {

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PictureHref)
        parcel.writeString(Category)
        parcel.writeInt(ListingLength)
        parcel.writeString(Suburb)
        parcel.writeInt(PromotionId)
        parcel.writeDouble(BuyNowPrice)
        parcel.writeString(Title)
        parcel.writeByte(if (IsNew) 1 else 0)
        parcel.writeByte(if (HasBuyNow) 1 else 0)
        parcel.writeString(EndDate)
        parcel.writeByte(if (IsBuyNowOnly) 1 else 0)
        parcel.writeString(StartDate)
        parcel.writeInt(ReserveState)
        parcel.writeString(NoteDate)
        parcel.writeString(Subtitle)
        parcel.writeString(CategoryPath)
        parcel.writeInt(ListingId)
        parcel.writeString(Region)
        parcel.writeByte(if (HasGallery) 1 else 0)
        parcel.writeString(AsAt)
        parcel.writeString(PriceDisplay)
        parcel.writeDouble(StartPrice)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ListItem> {
        override fun createFromParcel(parcel: Parcel): ListItem {
            return ListItem(parcel)
        }

        override fun newArray(size: Int): Array<ListItem?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            null,
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readDouble())

}

