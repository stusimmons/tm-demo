package nz.co.stuarts.trademe.features.listings

import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.base.MvpView

interface BrowseMvpView : MvpView {

    fun showListingDetail(listing: ListItem)
}