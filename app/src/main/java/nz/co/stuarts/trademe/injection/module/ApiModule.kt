package nz.co.stuarts.trademe.injection.module

import dagger.Module
import dagger.Provides
import nz.co.stuarts.trademe.data.remote.TradeMeApi
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by shivam on 8/7/17.
 */
@Module(includes = arrayOf(NetworkModule::class))
class ApiModule {

    @Provides
    @Singleton
    internal fun provideTrademeApi(retrofit: Retrofit): TradeMeApi =
            retrofit.create(TradeMeApi::class.java)
}