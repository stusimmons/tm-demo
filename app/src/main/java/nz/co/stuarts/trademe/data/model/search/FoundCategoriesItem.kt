package nz.co.stuarts.trademe.data.model.search

data class FoundCategoriesItem(val CategoryId: Int = 0,
                               val Category: String = "",
                               val Count: Int = 0,
                               val Name: String = "")