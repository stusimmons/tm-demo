package nz.co.stuarts.trademe.features.main

import nz.co.stuarts.trademe.data.DataManager
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.injection.ConfigPersistent
import nz.co.stuarts.trademe.features.base.BasePresenter
import nz.co.stuarts.trademe.features.categories.CategoriesMvpView
import nz.co.stuarts.trademe.util.rx.scheduler.SchedulerUtils
import javax.inject.Inject

@ConfigPersistent
class CategoriesPresenter @Inject
constructor(private val mDataManager: DataManager) : BasePresenter<CategoriesMvpView>() {

    fun getCategories(number: String) {
        checkViewAttached()
        mvpView?.showProgress(true)

        if (number.isBlank()) {
            mDataManager.getCategories()
                    .compose(SchedulerUtils.ioToMain<List<Subcategory>>())
                    .subscribe({ categories ->
                        mvpView?.showProgress(false)
                        mvpView?.showCategories(categories)
                    }) { throwable ->
                        mvpView?.showProgress(false)
                        mvpView?.showError(throwable)
                    }
        } else {
            mDataManager.getCategory(number)
                    .compose(SchedulerUtils.ioToMain<Subcategory>())
                    .subscribe({ categories ->
                        mvpView?.showProgress(false)
                        categories.Subcategories?.let { mvpView?.showCategories(it) }
                    }) { throwable ->
                        mvpView?.showProgress(false)
                        mvpView?.showError(throwable)
                    }
        }
    }

    fun checkConnection(networkConnected: Boolean) {
        if (networkConnected) mvpView?.showConnected() else mvpView?.showNoConnection()
    }
}