package nz.co.stuarts.trademe.features.listings.list

import nz.co.stuarts.trademe.data.DataManager
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.base.BasePresenter
import nz.co.stuarts.trademe.injection.ConfigPersistent
import nz.co.stuarts.trademe.util.rx.scheduler.SchedulerUtils
import javax.inject.Inject

@ConfigPersistent
class ListingsPresenter @Inject
constructor(private val mDataManager: DataManager) : BasePresenter<ListingsMvpView>() {

    fun getListings(category: String) {
        checkViewAttached()
        mvpView?.showProgress(true)
        mDataManager.getListingsForCategory(category)
                .compose(SchedulerUtils.ioToMain<List<ListItem>>())
                .subscribe({ listings ->
                    mvpView?.showProgress(false)
                    if (listings.isEmpty()){
                        mvpView?.showListingsEmpty()
                    } else {
                        mvpView?.showListings(listings)
                    }
                }) { throwable ->
                    mvpView?.showProgress(false)
                    mvpView?.showError(throwable)
                }
    }
}