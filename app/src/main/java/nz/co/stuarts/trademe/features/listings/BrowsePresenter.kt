package nz.co.stuarts.trademe.features.listings

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.base.BasePresenter
import nz.co.stuarts.trademe.injection.ConfigPersistent
import nz.co.stuarts.trademe.util.rx.scheduler.SchedulerUtils
import timber.log.Timber
import javax.inject.Inject

@ConfigPersistent
class BrowsePresenter @Inject
constructor() : BasePresenter<BrowseMvpView>() {

    private var subscriptions: CompositeDisposable? = null

    override fun attachView(mvpView: BrowseMvpView) {
        super.attachView(mvpView)
        subscriptions = CompositeDisposable()
    }

    fun addListingListener(listener: Observable<ListItem>) {
        subscriptions?.add(listener
                .compose(SchedulerUtils.ioToMain<ListItem>())
                .subscribe(
                        { it -> mvpView?.showListingDetail(it) },
                        { error -> Timber.e(error) }
                ))
    }
}