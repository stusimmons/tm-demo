package nz.co.stuarts.trademe.features.categories

import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.features.base.MvpView

interface CategoriesMvpView : MvpView {

    fun showCategories(categories: List<Subcategory>)

    fun showProgress(show: Boolean)

    fun showError(error: Throwable)

    fun showNoConnection()

    fun showConnected()
}