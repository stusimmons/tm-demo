package nz.co.stuarts.trademe.features.main

import nz.co.stuarts.trademe.R
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.util.loadImageFromUrl
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ListingsAdapter @Inject
constructor() : RecyclerView.Adapter<ListingsAdapter.ListingsViewHolder>() {

    private var mListings: List<ListItem>? = null
    private var mClickListener: ClickListener? = null

    init {
        mListings = emptyList()
    }

    fun setListings(listings: List<ListItem>) {
        mListings = listings
    }

    fun setClickListener(clickListener: ClickListener) {
        mClickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListingsViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_listing, parent, false)
        return ListingsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListingsViewHolder, position: Int) {
        val listing = mListings?.get(position)
        holder.mListing = listing
        holder.headingText?.text = listing?.Title
        holder.subheadingText?.text = listing?.Subtitle
        if (listing?.PictureHref != null) {
            holder.image?.loadImageFromUrl(listing.PictureHref)
        } else {
            holder.image?.visibility = View.INVISIBLE
        }
    }

    override fun getItemCount(): Int {
        return mListings?.size as Int
    }

    interface ClickListener {
        fun onItemClick(listing: ListItem)
    }

    open inner class ListingsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mListing: ListItem? = null
        @BindView(R.id.text_heading) @JvmField var headingText: TextView? = null
        @BindView(R.id.text_subheading) @JvmField var subheadingText: TextView? = null
        @BindView(R.id.image) @JvmField var image: ImageView? = null

        init {
            ButterKnife.bind(this, itemView)
            RxView.clicks(itemView)
                    .debounce(500, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ if (mClickListener != null) mClickListener?.onItemClick(mListing as ListItem) })
        }
    }
}
