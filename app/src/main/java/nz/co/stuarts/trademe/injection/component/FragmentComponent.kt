package nz.co.stuarts.trademe.injection.component

import dagger.Subcomponent
import nz.co.stuarts.trademe.features.listings.listing.detail.DetailFragment
import nz.co.stuarts.trademe.features.categories.CategoriesFragment
import nz.co.stuarts.trademe.features.main.ListingsFragment
import nz.co.stuarts.trademe.injection.PerFragment
import nz.co.stuarts.trademe.injection.module.FragmentModule

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Subcomponent(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
    fun inject(categoriesFragment: CategoriesFragment)
    fun inject(listingsFragment: ListingsFragment)
    fun inject(detailFragment: DetailFragment)
}