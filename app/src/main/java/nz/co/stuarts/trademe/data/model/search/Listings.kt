package nz.co.stuarts.trademe.data.model.search

data class Listings(val FoundCategories: List<FoundCategoriesItem>?,
                    val TotalCount: Int = 0,
                    val PageSize: Int = 0,
                    val DidYouMean: String = "",
                    val Page: Int = 0,
                    val List: List<ListItem>)