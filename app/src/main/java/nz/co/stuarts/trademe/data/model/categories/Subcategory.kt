package nz.co.stuarts.trademe.data.model.categories

/**
 * Created by stu on 17/12/17.
 */
data class Subcategory(
        val Name: String, //Trade Me Motors
        val Number: String, //0001-
        val Path: String, ///Trade-Me-Motors
        val Subcategories: List<Subcategory>?,
        val HasClassifieds: Boolean, //true
        val CanHaveSecondCategory: Boolean, //true
        val CanBeSecondCategory: Boolean, //true
        val IsLeaf: Boolean //false
)