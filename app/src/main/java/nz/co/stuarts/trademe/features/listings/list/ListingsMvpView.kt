package nz.co.stuarts.trademe.features.listings.list

import nz.co.stuarts.trademe.data.model.search.ListItem
import nz.co.stuarts.trademe.features.base.MvpView

interface ListingsMvpView : MvpView {

    fun showProgress(show: Boolean)

    fun showError(error: Throwable)

    fun showListings(listings: List<ListItem>)

    fun showListingsEmpty()
}