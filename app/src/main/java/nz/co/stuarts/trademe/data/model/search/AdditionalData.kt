package nz.co.stuarts.trademe.data.model.search

data class AdditionalData(
		val BulletPoints: List<Any>,
		val Tags: List<Any>
)