package nz.co.stuarts.trademe.features.categories

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import nz.co.stuarts.trademe.R
import nz.co.stuarts.trademe.data.model.categories.Subcategory
import nz.co.stuarts.trademe.features.base.BaseFragment
import nz.co.stuarts.trademe.features.common.ErrorView
import nz.co.stuarts.trademe.features.main.CategoriesPresenter
import nz.co.stuarts.trademe.util.NetworkUtil
import javax.inject.Inject


class CategoriesFragment : BaseFragment(), CategoriesMvpView, CategoriesAdapter.ClickListener, ErrorView.ErrorListener {

    @Inject lateinit var mCategoriesAdapter: CategoriesAdapter
    @Inject lateinit var mPresenter: CategoriesPresenter

    private var categoryNumber: String = ""

    @BindView(R.id.view_error) @JvmField var mErrorView: ErrorView? = null
    @BindView(R.id.progress) @JvmField var mProgress: ProgressBar? = null
    @BindView(R.id.recycler) @JvmField var mCategoriesRecycler: RecyclerView? = null
    @BindView(R.id.swipe_to_refresh) @JvmField var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    @BindView(R.id.layout_root) @JvmField var snackbarAnchor: LinearLayout? = null

    private var notifyCategories = PublishSubject.create<Subcategory>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(layout, container, false)
        fragmentComponent().inject(this)
        ButterKnife.bind(this, view)

        mPresenter.attachView(this)

        readBundle(arguments)
        mSwipeRefreshLayout?.setProgressBackgroundColorSchemeResource(R.color.primary)
        mSwipeRefreshLayout?.setColorSchemeResources(R.color.white)
        mSwipeRefreshLayout?.setOnRefreshListener { mPresenter.getCategories(categoryNumber)}
        mCategoriesRecycler?.adapter = mCategoriesAdapter
        mCategoriesAdapter.setClickListener(this)
        mCategoriesRecycler?.layoutManager = LinearLayoutManager(context)

        mErrorView?.setErrorListener(this)

        mPresenter.checkConnection(NetworkUtil.isNetworkConnected(context))
        return view
    }

    override fun showConnected() {
        mPresenter.getCategories(categoryNumber)
    }

    private fun readBundle(bundle: Bundle?): String? {
        if (bundle != null) {
            categoryNumber = bundle.getString("number")
        }
        return categoryNumber
    }

    companion object {
        fun newInstance(number: String): CategoriesFragment {
            val bundle = Bundle()
            bundle.putString("number", number)
            val fragment = CategoriesFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun showNoConnection() {
        mCategoriesRecycler?.visibility = View.GONE
        mSwipeRefreshLayout?.visibility = View.GONE
        mErrorView?.setMessage(getString(R.string.error_no_connection))
        mErrorView?.visibility = View.VISIBLE
    }

    override val layout: Int
        get() = R.layout.fragment_recycler

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
    }

    override fun showCategories(categories: List<Subcategory>) {
        mCategoriesAdapter.setCategories(categories)
        mCategoriesAdapter.notifyDataSetChanged()

        mCategoriesRecycler?.visibility = View.VISIBLE
        mSwipeRefreshLayout?.visibility = View.VISIBLE
    }


    override fun showProgress(show: Boolean) {
        if (show) {
            if (mCategoriesRecycler?.visibility == View.VISIBLE && mCategoriesAdapter.itemCount > 0) {
                mSwipeRefreshLayout?.isRefreshing = true
            } else {
                mProgress?.visibility = View.VISIBLE

                mCategoriesRecycler?.visibility = View.GONE
                mSwipeRefreshLayout?.visibility = View.GONE
            }

            mErrorView?.visibility = View.GONE
        } else {
            mSwipeRefreshLayout?.isRefreshing = false
            mProgress?.visibility = View.GONE
        }
    }

    override fun showError(error: Throwable) {
        mCategoriesRecycler?.visibility = View.GONE
        mSwipeRefreshLayout?.visibility = View.GONE
        mErrorView?.visibility = View.VISIBLE
    }

    override fun onReloadData() {
        mPresenter.getCategories(categoryNumber)
    }

    override fun onItemClick(callback: Subcategory?) {
        callback?.let { notifyCategories.onNext(it) }
    }

    fun categoriesObservable(): Observable<Subcategory> {
        return notifyCategories
    }
}