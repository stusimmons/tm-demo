package nz.co.stuarts.trademe.injection.component

import nz.co.stuarts.trademe.injection.PerActivity
import nz.co.stuarts.trademe.injection.module.ActivityModule
import nz.co.stuarts.trademe.features.base.BaseActivity
import dagger.Subcomponent
import nz.co.stuarts.trademe.features.listings.BrowseActivity
import nz.co.stuarts.trademe.features.main.MainActivity

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(baseActivity: BaseActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(browseActivity: BrowseActivity)

}
